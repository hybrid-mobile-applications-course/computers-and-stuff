import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import ProductItem from '../../components/shop/ProductItem';
import Colors from '../../constants/Colors';
import * as productsActions from '../../store/actions/products';

const UserProductsScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const userProducts = useSelector(state => state.products.userProducts);
  const dispatch = useDispatch();

  const editProductHandler = (id) => {
    props.navigation.navigate('EditProduct', {productId: id});
  };

  const deleteHandler = (id) => {
    Alert.alert('Confirm deletion', 'Item will be deleted. Proceed?', [
      {text: 'Cancel', style: 'default'},
      {
        text: 'Confirm', style: 'destructive', onPress: () => {
          setIsLoading(true);
          dispatch(productsActions.deleteProduct(id));
          setIsLoading(false);
        },
      },
    ]);
  };

  if (isLoading) {
    return (
        <View style={styles.center}>
          <ActivityIndicator size='large' color={Colors.primary}/>
        </View>
    );
  }

  if (userProducts.length === 0) {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>No listings found. Maybe add some?</Text>
        </View>
    );
  }

  return <FlatList
      data={userProducts}
      renderItem={
        itemData =>
            <ProductItem
                imageUrl={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={() => {
                  editProductHandler(itemData.item.id);
                }}
            >
              <Button color={Colors.primary} title='Edit'
                      onPress={() => {
                        editProductHandler(itemData.item.id);
                      }}/>
              <Button color={Colors.primary}
                      title='Delete'
                      onPress={
                        deleteHandler.bind(
                            this,
                            itemData.item.id,
                        )}/>
            </ProductItem>}/>;
};

UserProductsScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Your Listings',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Menu"
              iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
          />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Add"
              iconName={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
              onPress={() => {
                navData.navigation.navigate('EditProduct');
              }}
          />
        </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default UserProductsScreen;
