import React, {
  useCallback,
  useEffect,
  useReducer,
  useRef,
  useState,
} from 'react';
import {
  ActivityIndicator,
  Alert,
  Animated,
  Button,
  Easing,
  KeyboardAvoidingView,
  StyleSheet,
  View,
} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import {useDispatch} from 'react-redux';

import Input from '../../components/UI/Input';
import Card from '../../components/UI/Card';
import Colors from '../../constants/Colors';

import * as authenticationActions from '../../store/actions/authentication';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    };
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    };
    let updatedFormIsValid = true;
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    };
  }
  return state;
};

const AuthScreen = props => {

  const [opened, setOpened] = useState(false);

  const animatedValue = new Animated.Value(0);
  const animatedValueRef = useRef(animatedValue);

  const translateX = animatedValueRef.current.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 600],
  });

  function showSearchWithUseState() {
    Animated.timing(animatedValueRef.current, {
      toValue: 1,
      duration: 150,
      useNativeDriver: true,
      easing: Easing.linear,
    }).start(() => {
      setOpened(true);
    });
  }

  function hideSearchWithUseState() {
    Animated.timing(animatedValueRef.current, {
      toValue: 0,
      duration: 100,
      useNativeDriver: true,
      easing: Easing.linear,
    }).start(() => {
      setOpened(false);
    });
  }

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [isRegister, setIsRegister] = useState(false);
  const dispatch = useDispatch();

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      email: '',
      password: '',
      displayName: '',
    },
    inputValidities: {
      email: false,
      password: false,
      displayName: false,
    },
    formIsValid: false,
  });

  useEffect(() => {
    if (error) {
      Alert.alert('Error', error, [{text: 'OK'}]);
    }
  }, [error]);

  const registerAndLoginHandler = async () => {
    let action;
    setError(null);
    if (isRegister) {
      action = authenticationActions.register(
          formState.inputValues.email,
          formState.inputValues.password,
          formState.inputValues.displayName,
      );
    } else {
      action = authenticationActions.login(
          formState.inputValues.email,
          formState.inputValues.password,
      );
    }
    setIsLoading(true);

    try {
      await dispatch(action);
      props.navigation.navigate('Shop');
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  };

  const inputChangeHandler = useCallback(
      (inputIdentifier, inputValue, inputValidity) => {
        dispatchFormState({
          type: FORM_INPUT_UPDATE,
          value: inputValue,
          isValid: inputValidity,
          input: inputIdentifier,
        });
      },
      [dispatchFormState],
  );

  return (
      <KeyboardAvoidingView
          keyboardVerticalOffset={50}
          style={styles.screen}
      >
        <LinearGradient colors={['#000428', '#004e92']} style={styles.gradient}>
          <Card style={styles.authContainer}>
            {!opened ? (
                <Button title='Show login form'
                        onPress={showSearchWithUseState}/>
            ) : (
                <Button title='Hide login form'
                        onPress={hideSearchWithUseState}/>
            )}
            <Animated.ScrollView
                style={{
                  transform: [{translateX}],
                }}
            >
              <Input
                  id="email"
                  label="E-Mail"
                  keyboardType="email-address"
                  required
                  email
                  autoCapitalize="none"
                  errorText="Please enter a valid email address."
                  onInputChange={inputChangeHandler}
                  initialValue=""
              />
              <Input
                  id="password"
                  label="Password"
                  keyboardType="default"
                  secureTextEntry
                  required
                  minLength={5}
                  autoCapitalize="none"
                  errorText="Password must be at least 6 characters long."
                  onInputChange={inputChangeHandler}
                  initialValue=""
              />

              {isRegister ? (
                      <Input
                          id="displayName"
                          label="Phone"
                          keyboardType="phone-pad"
                          required
                          errorText="Please enter phone number."
                          onInputChange={inputChangeHandler}
                          initialValue=""
                      />
                  ) :
                  (<View/>)
              }

              {isLoading ? (
                  <View style={styles.loadingView}>
                    <ActivityIndicator size="large" color={Colors.primary}/>
                  </View>
              ) : (
                  <View style={styles}>
                    <View style={styles.buttonContainer}>
                      <Button
                          title={isRegister ? 'Register' : 'Login'}
                          color={Colors.primary}
                          onPress={registerAndLoginHandler}
                      />
                    </View>
                    <View style={styles.buttonContainer}>
                      <Button
                          title={`Go to ${isRegister ? 'Login' : 'Register'}`}
                          color={Colors.royal}
                          onPress={() => {
                            setIsRegister(prevState => !prevState);
                          }}
                      />
                    </View>
                  </View>
              )}
            </Animated.ScrollView>
          </Card>
        </LinearGradient>
      </KeyboardAvoidingView>
  );
};

AuthScreen.navigationOptions = {
  headerTitle: 'Login',
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    maxHeight: 400,
    padding: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
  loadingView: {
    marginTop: 20,
  },
});

export default AuthScreen;
