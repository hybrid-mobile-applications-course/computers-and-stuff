import React from 'react';
import {
  Button,
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import Colors from '../../constants/Colors';

const ProductDetailScreen = props => {
  const productId = props.navigation.getParam('productId');
  const phone = props.navigation.getParam('phone');
  const selectedProduct = useSelector(state =>
      state.products.availableProducts.find(prod => prod.id === productId));

  const dialCall = () => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = `tel:${phone}`;
    } else {
      phoneNumber = `telprompt:${phone}`;
    }

    Linking.openURL(phoneNumber);
  };

  return (
      <ScrollView>
        <Image style={styles.image} source={{uri: selectedProduct.imageUrl}}/>
        <Text style={styles.price}>{selectedProduct.price.toFixed(2)}€</Text>
        <Text style={styles.description}>{selectedProduct.description}</Text>
        <View style={styles.buttonContainer}>
          <Button
              title={'Call ' + phone}
              color={Colors.primary}
              onPress={dialCall}
          />
        </View>
      </ScrollView>
  );
};

ProductDetailScreen.navigationOptions = navData => {
  return {
    headerTitle: navData.navigation.getParam('productTitle'),
  };
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300,
  },
  price: {
    fontFamily: 'open-sans-bold',
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20,
  },
  description: {
    fontFamily: 'open-sans',
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20,
  },
  buttonContainer: {
    marginTop: 10,
  },
});

export default ProductDetailScreen;
