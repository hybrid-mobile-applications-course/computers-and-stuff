import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer';
import {Button, Platform, SafeAreaView, View} from 'react-native';
import {Ionicons} from '@expo/vector-icons';

import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import UserProductsScreen from '../screens/user/UserProductsScreen';
import EditProductScreen from '../screens/user/EditProductScreen';
import AuthScreen from '../screens/user/AuthScreen';
import Colors from '../constants/Colors';
import * as authenticationActions from '../store/actions/authentication';
import {useDispatch} from 'react-redux';
import SplashScreen from '../screens/SplashScreen';

const defaultNavOptions = {
  headerTintColor: Colors.primary,
  headerTitleStyle: {
    fontFamily: 'open-sans-bold',
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans',
  },
};

const ProductsNavigator = createStackNavigator({
      ProductsOverview: ProductsOverviewScreen,
      ProductDetail: ProductDetailScreen,
    }, {
      navigationOptions: {
        drawerIcon: drawerConfig => (
            <Ionicons
                name={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'}
                size={23}
                color={drawerConfig.tintColor}
            />
        ),
      },
      defaultNavigationOptions: defaultNavOptions,
    },
);

const AdminNavigator = createStackNavigator(
    {
      UserProducts: UserProductsScreen,
      EditProduct: EditProductScreen,
    },
    {
      navigationOptions: {
        drawerIcon: drawerConfig => (
            <Ionicons
                name={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
                size={23}
                color={drawerConfig.tintColor}
            />
        ),
      },
      defaultNavigationOptions: defaultNavOptions,
    },
);

const ShopNavigator = createDrawerNavigator(
    {
      'All listings': ProductsNavigator,
      'My listings': AdminNavigator,
    },
    {
      contentOptions: {
        activeTintColor: Colors.primary,
      },
      contentComponent: props => {
        const dispatch = useDispatch();
        return (
            <View style={{flex: 1, paddingTop: 30}}>
              <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
                <DrawerItems {...props} />
                <Button
                    title="Logout"
                    color={Colors.primary}
                    onPress={() => {
                      dispatch(authenticationActions.logout());
                      props.navigation.navigate('Auth');
                    }}
                />
              </SafeAreaView>
            </View>
        );
      },
    },
);

const AuthNavigator = createStackNavigator(
    {
      Auth: AuthScreen,
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#000428',
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontFamily: 'open-sans-bold',
        },
        headerBackTitleStyle: {
          fontFamily: 'open-sans',
        },
      },
    },
);

const PrimaryNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  Auth: AuthNavigator,
  Shop: ShopNavigator,
});

export default createAppContainer(PrimaryNavigator);
